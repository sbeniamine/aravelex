%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%            post-processing rules               %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%%% 1. Pharyngalized s
% I can't find any source for this, leaving it aside for now.
% s -> sˤ / [aiuː]+ _ [aiuː]+[ktʕq]ˤ?[aiuː]+[tmn]ˤ?


%%% 1. Initial glottal stop

%% Add a dummy symbol for the ones we want to keep

0 -> § / # _ ʔ[aiu](bb|θθ|dd|rr|ss|kk|ll|mm|ww|tt|ðð|tˤtˤ)
0 -> § / # _ ʔ[aiu]ː

%% Remove others
ʔ -> 0 / # _

%% Remove dummy symbol
§ -> 0 / _
