from paralex import paralex_factory

package = paralex_factory("AraVeLex: Modern Standard Arabic Verbal lexicon",
                          {"cells": {"path": "std_modern_arabic_cells.csv"},
                           "forms": {"path": "std_modern_arabic_paradigms.csv"},
                           "lexemes": {"path": "std_modern_arabic_lexemes.csv"},
                           "sounds": {"path": "std_modern_arabic_sounds.csv"},
                           "features-values": {"path": "std_modern_arabic_features.csv"}
                           },
                          contributors=[{'title': 'Sacha Beniamine', 'role': 'author', }, ],
                          licenses=[{'name': 'CC-BY-SA-3.0',
                                     'title': 'Attribution-ShareAlike 3.0 Unported (CC BY-SA 3.0) ',
                                     'path': 'https://creativecommons.org/licenses/by-sa/3.0/'}],
                          keywords=["arabic", "verbs", "paradigms", "paralex"],
                          name="aravelex",
                          version="1.0.1",
                          languages_iso639=["arb"],
                          related_identifiers={
                              'relation': 'isDerivedFrom',
                              'identifier': 'https://github.com/unimorph/ara/blob/master/ara'
                          },
                          )

package.infer()
package.to_json("paralex_std_modern_arabic.package.json")
