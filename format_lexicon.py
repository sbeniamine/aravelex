#!/usr/bin/env python
# coding: utf-8
import pandas as pd
import re
from tqdm import tqdm
from shutil import copyfile
import epitran
from pathlib import Path
import logging
from importlib_resources import files
from slugify import slugify
tqdm.pandas()




def add_rules(dir, code, reverse=False):
    """ Move rules files to Epitran's data directory

    Args:
        dir (str): path to the directory where the rules are stored.
        code (str): ISO 639-3 plus "-" plus ISO 15924 code of the
                    language/script pair that should be loaded
        reverse (bool): whether to also move reverse files
    """
    paths = [f"map/{code}.csv",
             f"pre/{code}.txt",
             f"post/{code}.txt"]

    if reverse:
        paths.extend([f"map/{code}_rev.csv", f"pre/{code}_rev.txt",
                      f"post/{code}_rev.txt"])
    for path in paths:
        ref = files('epitran').joinpath(f"data/{path}")
        copyfile(f"{dir}/{path}", ref)


def get_epitran_instance(code="ara-Latn"):
    rule_folder = Path("./epitran").resolve()
    if (rule_folder / "map" / (code + ".csv")).exists():
        add_rules(rule_folder, code, reverse=False)
    epi = epitran.Epitran(code)
    return epi


def parse_form(value):
    if value in {"—", "?"} or pd.isna(value):
        return ["#DEF# #DEF#"]
    # This is a bit absurd, but some have "or or" in between, and some have just "or".
    orth, roman = value.replace(" or or ", ";").replace(" or ", ";").split(" ")
    roman = roman.split(";")
    orth = orth.split(";")
    forms = zip(roman, orth)
    return [" ".join(x) for x in forms]


def parse_unimorph_tag(row):
    row["Person"] = str(int(row["Person"])) if not pd.isna(row["Person"]) else row["Person"]
    tag = []
    for d in ["Part of Speech", "Person", "Number", "Gender", "Tense", "Aspect", "Mood", "Voice"]:
        v = row[d]
        if v and not pd.isna(v):
            tag.append(v)
    return ";".join(tag)


def process_lexicon():
    unimorph = pd.read_csv("raw/UNIMORPHArabic_table.csv")

    # Filtering
    unimorph = unimorph[
        unimorph["Part of Speech"].isin({"V"}) & unimorph["cell_value"].str.contains(r"[ —?]", regex=True)]
    unimorph.dropna(axis=1, how="all", inplace=True)

    # Parsing paradigm cells
    cell_mapping = pd.read_csv("std_modern_arabic_cells.csv")
    unimorph["unimorph_cell"] = unimorph.apply(parse_unimorph_tag, axis=1)
    to_ours = dict(cell_mapping[["unimorph_cell_old", "cell_id"]].to_records(index=False))
    unimorph["cell"] = unimorph["unimorph_cell"].map(to_ours)

    # Handle overabundance and separate roman & arabic forms
    unimorph.loc[:, "cell_value"] = unimorph.cell_value.apply(parse_form)
    unimorph = unimorph.explode("cell_value").reset_index(drop=True)
    forms = pd.DataFrame(unimorph["cell_value"].str.split(" ").to_list(), columns=['orth_roman_form', 'orth_form'])
    unimorph = unimorph.join(forms)
    unimorph = unimorph[["page_url", "cell", "orth_roman_form", "orth_form"]]

    # Adding lexeme info...
    # Unfortunately, there is sometimes several lexemes on the same page
    # These are not directly distinguished, but they can be detected, because the
    # paradigms usually start and end with the same cells...
    # Thus, there is a new paradigm when either the URL changes...
    url_change = unimorph['page_url'] != unimorph['page_url'].shift(1)
    # Or we switch from the last cell (jussive f3p) to the first one (perf m1s)
    first_form = (unimorph['cell'] == "ind.prf.act.m.1.s") & (unimorph['cell'].shift(1) == "juss.pass.f.3.p")
    new_paradigm = (url_change | first_form)

    # We use this information to add indexes:
    unimorph.loc[:, "lexeme_index"] = new_paradigm.cumsum()

    # We now assign the ind.prf.act.m.3.s  orth_form and orth_roman_form as a lemmas
    def get_lemma(group):
        lemmas = group[group.cell == "ind.prf.act.m.3.s"]
        row = lemmas.iloc[0, :]
        return (group.name, row.orth_form, row.orth_roman_form)

    lemma_key = {a: (b,c) for a, b, c in unimorph.groupby("lexeme_index").apply(get_lemma).values}
    lemmas = pd.DataFrame(unimorph["lexeme_index"].map(lemma_key).tolist(), columns=["lemma", "lemma_roman"])
    unimorph = unimorph.join(lemmas)

    # A more aggressive romanisation to be used in the lexeme IDs
    def my_slug(form):
        changes = [("ṯ","th"),
                   ("ʾ", "2"),
                   ("ā", "aa"),
                   ("ḵ", "kh"),
                   ("ḏ", "dh"),
                   ("š", "sh"),
                   ("ḡ", "gh"),
                   ("ū", "uu"),
                   ("ō", "oo"),
                   ("ī", "ii"),
                   ("ē", "ee"),
                   ]
        for a,b in changes:
            form = form.replace(a,b)
        return slugify(form)
    # Create unambiguous identifiers
    unimorph["lexeme"] = "lex_" + unimorph["lexeme_index"].apply(str) + "_" + unimorph["lemma_roman"].apply(my_slug)


    lexemes = unimorph[unimorph.cell == "ind.prf.act.m.3.s"]
    lexemes = lexemes[["lexeme","lemma",  "lemma_roman"]]
    lexemes.columns = ["lexeme_id", "lemma", "lemma_roman"]
    lexemes["POS"] = "verb"
    lexemes.reset_index(drop=True, inplace=True)
    lexemes.drop_duplicates(inplace=True)

    unimorph.drop(["lemma", "lemma_roman", "page_url", "lexeme_index"], inplace=True, axis=1)
    unimorph.drop_duplicates(inplace=True)
    unimorph.reset_index(drop=True, inplace=True)
    unimorph.index = ["form_" + str(x) for x in unimorph.index]
    unimorph.index.name = "form_id"

    # Phonological transcription
    epi = get_epitran_instance()
    logging.disable(logging.DEBUG)

    def transcribe(form):
        if pd.isna(form) or form == "#DEF#":
            return form
        return epi.transliterate(form)

    unimorph["phon_form"] = unimorph["orth_roman_form"].progress_apply(transcribe)
    unimorph = unimorph.drop_duplicates()

    def separate_sounds(form, seg):
        return " ".join([x for x in seg.split(form) if x])

    reg = re.compile(r"(dˤ|sˤ|tˤ|ðˤ|aː|iː|uː|h|j|w|b|d|f|k|l|m|n|q|r|s|t|x|z|ð|ħ|ɣ|ʃ|ʔ|ʕ|ʤ|θ|a|i|u)")
    unimorph["phon_form"] = unimorph["phon_form"].apply(lambda x: separate_sounds(x, reg) if x != "#DEF#" else x)

    unimorph.to_csv("std_modern_arabic_paradigms.csv", index=True)
    lexemes.to_csv("std_modern_arabic_lexemes.csv", index=False)


if __name__ == "__main__":
    process_lexicon()
